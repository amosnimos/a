# X is A custom config Installer.
echo -ne "\e[0;31m\n"
echo "  XXXX    XXXX" 
echo "  XXXX    XXXX"
echo "   XXXX  XXXX "
echo "    XXXXXXXX  "
echo "     XXXXXX   "
echo "    XXXXXXXX  "
echo "   XXXX  XXXX "
echo "  XXXX    XXXX"
echo "  XXXX    XXXX" 
echo -ne "\e[0m\n"

function install(){
	
	echo -ne "\e[0;31;43m [Update apt.] \e[0m\n"
	read -p "Proceed? [y/N]: " response
	case "$response" in
	[yY][eE][sS]|[yY]) 
		sudo apt update
		;;
	esac
	
	# Option 0
	echo -ne "\e[0;31;43m [Install awesome.] \e[0m\n"
	read -p "Proceed? [y/N]: " response
	case "$response" in
	[yY][eE][sS]|[yY]) 
		sudo apt update && sudo apt install -y awesome compton
		;;
	esac
	
	# Option 1
	echo -ne "\e[0;31;43m [Install Project-A.] \e[0m\n"
	
	read -p "Proceed? [y/N]: " response
	case "$response" in
	[yY][eE][sS]|[yY]) 
		local outdir=/var/tmp/amosnimos_config
		git clone "https://gitlab.com/amosnimos/a.git" "${outdir}"
		echo -ne "\e[0;31;43m [Export bashrc file.] \e[0m\n"
		mv "${outdir}/.bashrc" "/home/$USER" && echo "Done."
		echo -ne "\e[0;31;43m [Export shell directory.] \e[0m\n"
		mv "${outdir}/.shell" "/home/$USER" && mv "${outdir}/.config/.nanorc" "/home/$USER" && echo "Done."
		echo -ne "\e[0;31;43m [Export awesome config.] \e[0m\n"
		mv "${outdir}/.config/awesome" "/home/$USER/.config" && rm -fr "${outdir}" && echo "Done."
		;;
	esac
	
	# Option 2
	echo -ne "\e[0;31;43m [Install functional program.] \e[0m\n"
	
	read -p "Proceed? [y/N]: " response
	case "$response" in
	[yY][eE][sS]|[yY]) 
		sudo apt update && sudo apt install -y fzf unzip git nano vim transmission blueman python3-pip suckless-tools sox ffmpeg neofetch espeak mpv cmus bc sc ranger bash-completion
		;;
	esac

	# Option 3
	echo -ne "\e[0;31;43m [Install optional program.] \e[0m\n"
	
	read -p "Proceed? [y/N]: " response
	case "$response" in
	[yY][eE][sS]|[yY]) 
		sudo apt update && sudo apt install -y xsel w3m gedit lxappearance rofi obs-studio pavucontrol youtube-dl calibre libreoffice festival moc wkhtmltopdf gimp inkscape jp2a xdotool cmatrix tty-clock
		;;
	esac
	
	# Option 4
	echo -ne "\e[0;31;43m [Install games.] \e[0m\n"
	
	read -p "Proceed? [y/N]: " response
	case "$response" in
	[yY][eE][sS]|[yY]) 
		sudo apt update && sudo apt install bastet ninvaders pacman4console nsnake greed moon-buggy
		sudo apt update && sudo apt install -y hedgewars supertux pingus teeworlds
		sudo apt update && sudo apt install -y minetest supertuxkart assaultcube redeclipse sauerbraten
		;;
	esac
	
	# Option 5
	echo -ne "\e[0;31;43m [Install CAVA.] \e[0m\n"
	
	read -p "Proceed? [y/N]: " response
	case "$response" in
	[yY][eE][sS]|[yY]) 
		sudo add-apt-repository ppa:hsheth2/ppa
		sudo apt update
		sudo apt install cava
		;;
	esac
	
	# Option 6
	echo -ne "\e[0;31;43m [Configure.] \e[0m\n"
	read -p "Proceed? [y/N]: " response
	case "$response" in
	[yY][eE][sS]|[yY]) 
		mkdir ~/.config/rofi/ && git clone https://github.com/AmosNimos/rofi-theme.git ~/.config/rofi/
		mkdir ~/.moc && cp /usr/share/doc/moc/examples/config.example ~/.moc && mv ~/.moc/config.example ~/.moc/config && echo "Theme = /usr/share/moc/themes/darkdot_theme" >> ~/.moc/config
		;;
	esac
	
	#Update
	echo Update youtube-dl
	pip install update youtube-dl
	sudo apt update
	sudo apt upgrade
	exit
}

install


