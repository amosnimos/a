# Automatically update the file in my gitlab directory with the one i use.

source ~/.config/pdata/gitlab.sh

lab_path=~/Documents/gitlab/a
comment="Update"
uplaod="y"

cd $lab_path

# flags
if [[ $# -gt 0 ]]; then
	upload="n"
	while getopts "sbtuahc:" opt; do
		case "${opt}" in
			s) rm -r $lab_path/.shell; cp -r ~/.shell $lab_path && echo "$lab_path/.shell Updated.";;
			b) cp -r ~/.bashrc $lab_path && echo "$lab_path/.bashrc Updated.";;
			t) dconf dump /org/gnome/terminal/legacy/profiles:/ > $lab_path/.theme/bash_profile && echo "$lab_path/.theme Updated.";;
			u) upload="y";;
			c) comment="${OPTARG}";;
			a) upload="y"
			   dconf dump /org/gnome/terminal/legacy/profiles:/ > $lab_path/.theme/bash_profile;
				 cp -r ~/.bashrc $lab_path
				 rm -r $lab_path/.shell; cp -r ~/.shell $lab_path
				 ;;
			\?)
			  echo "Invalid option: -${OPTARG}" >&2
			  echo "try: -h (h for Help)"
			  exit 1
			  ;;
			:)
			  echo "Option -$OPTARG requires an argument." >&2
			  exit 1
			  ;;
			h) 
				echo "[Options]"
				echo "  -s (s for .shell), update .shell"
				echo "  -b (b for .bashrc), update .bashrc"
				echo "  -t (t for .theme), update bash theme profiles"
				echo "  -u (u for upload), upload to gitlab"
				echo "  -a (a for all), does all of the above."
				echo "  -c (c for comment), commit comment."
				echo "  -h (h for help), display options."
				exit
				;;
		esac
	done
	OPTIND=1 # reset flags
fi

if [[ "${upload}" == "y" ]]; then 
	echo "$(pwd) commit upload."
	git add $lab_path
	git commit -m "${comment}"
	git push -uf origin main
fi

