# By amosnimos 2021
# https://gitlab.com/amosnimos

#### [FUNCTION]

## d
### Delete directory/file
d(){
	for arg in $@; do # aruments
		if [[ -d "${arg}" ]]; then # is directory
			if [ "$(ls -A ${arg})" ]; then # is not empty
				rm -r "${arg}"
			else # empty
				rm -d "${arg}"
			fi
		elif [[ -f "${arg}" ]]; then # is file
			rm "${arg}" 
		fi
	done
}

## o
### Open directory/file
o(){
	if [[ $# -gt 0 ]]; then
		for arg in $@; do # aruments
			if [[ -d "${arg}" ]]; then 
				cd $1
				clear
				echo "[$(pwd)]:"
				ls
			elif [[ -f "${arg}" ]]; then
				if [[ $(file --mime-type -b "${arg}") == text/* ]]; then
					nohup $VISUAL_EDITOR "${arg}" &> /tmp/nohup.out & disown ; clear
				elif [[ $(file --mime-type -b "${arg}") == image/* ]]; then # if not work remove g and try again.
					nohup feh "${arg}" &> /tmp/nohup.out & disown ; clear
				elif [[ $(file --mime-type -b "${arg}") == video/* ]]; then
					nohup mpv --no-sub "${arg}" &> /tmp/nohup.out & disown ; clear
				elif [[ $(file --mime-type -b "${arg}") == audio/* ]]; then
					mpv "${arg}"
				else 
					echo "$arg filetype undetected."
					$VISUAL_EDITOR "${arg}"
				fi
			else 
				echo "$arg No such file or directory"
			fi
		done
	else
		arg=$(ls -a | fzf --prompt "Open:")
		if [[ -d "${arg}" ]]; then 
			cd ${arg}
			clear
			echo "[$(pwd)]:"
			ls
		elif [[ -f "${arg}" ]]; then
			if [[ $(file --mime-type -b "${arg}") == text/* ]]; then
				nohup $VISUAL_EDITOR "${arg}" &> /tmp/nohup.out & disown ; clear
			elif [[ $(file --mime-type -b "${arg}") == image/* ]]; then # if not work remove g and try again.
				nohup feh "${arg}" &> /tmp/nohup.out & disown ; clear
			elif [[ $(file --mime-type -b "${arg}") == video/* ]]; then
				nohup mpv --no-sub "${arg}" &> /tmp/nohup.out & disown ; clear
			elif [[ $(file --mime-type -b "${arg}") == audio/* ]]; then
				mpv "${arg}"
			else 
				echo "$arg filetype undetected."
				$VISUAL_EDITOR "${arg}"
			fi
		else 
			echo "o: ${arg} No such file or directory"
		fi
	fi
}

## fav
### favorit directory/file.
f(){
	local location=/var/tmp/favorit_path.txt
	if [[ $# -gt 0 ]]; then
		while getopts 'seacrh' opt; do
			case "${opt}" in
				s) echo "Favorit:[${location}]"; cat "${location}";;
				e) echo "$(pwd)" >> ${location};;
				a)
				 selection="$(ls -a | fzf --prompt "Add:")"
				 echo "$(pwd)/${selection}" >> ${location}
				 ;;
				c) echo -e ".\n/home/$USER" > ${location};;
				r) 
				 selection="$(cat ${location} | fzf --prompt "Remove:")" 
				 sed -i "s~${selection}~~g" "${location}"
				 sed -i "/^$/d" "${location}"
				 ;;
				\?)
				  echo "Invalid option: -${OPTARG}" >&2
				  echo "Try: -h (for Help)"
				  ;;
				:)
				  echo "Option -$OPTARG requires an argument." >&2
				  ;;
				h) 
					echo "[Options]"
					echo "  -s (show favorit)."
					echo "  -c (clear favorit)."
					echo "  -e (export current directory to favorit)."
					echo "  -a (add favorit)."
					echo "  -r (remove directory)."
					echo "  -h (help)."
					;;
			esac
		done
		OPTIND=1 # reset flags
	else 
		selection="$(cat "${location}" | fzf --prompt "Open:")"
		echo "not work: ${selection}"
		if [[ -d "${selection}" ]]; then 
			cd ${selection}
			clear
			echo "[$(pwd)]:"
			ls
		elif [[ -f "${selection}" ]]; then
			if [[ $(file --mime-type -b "${selection}") == text/* ]]; then
				nohup $VISUAL_EDITOR "${selection}" &> /tmp/nohup.out & disown ; clear
			elif [[ $(file --mime-type -b "${selection}") == image/* ]]; then # if not work remove g and try again.
				nohup feh "${selection}" &> /tmp/nohup.out & disown ; clear
			elif [[ $(file --mime-type -b "${arg}") == video/* ]]; then
				nohup mpv --no-sub "${selection}" &> /tmp/nohup.out & disown ; clear
			elif [[ $(file --mime-type -b "${selection}") == audio/* ]]; then
				mpv "${selection}"
			fi
		else 
			echo "${selection} No such file/directory"
		fi
	fi
}


## shell
### Open a new terminal at the current working directory.
shell(){
	dir="$(pwd)"
	gnome-terminal -t "bash -c 'cd $dir; $SHELL'"
}

## run
### Run a command without outputing the result to terminal.
run(){
	nohup $1 &> /tmp/nohup.out &disown && clear
}

## go
### Open the terminal path with file manager
go(){
	nohup nautilus . &> /tmp/nohup.out & disown ; clear
}

## calc
### Calculate equation
calc(){
	echo "$1" | bc
}

## yt
### Download youtube video/audio
yt(){
	# Get flags
	if [[ $# -gt 0 ]]; then
		while getopts ":l:o:a:uh" opt; do
			case "${opt}" in
				l) url="${OPTARG}" ;;
				o) outdir="${OPTARG}" ;;
				a) selection="${OPTARG}" ;;
				u) pip3 install --upgrade youtube-dl; return 0 ;;
				\?)
				  echo "Invalid option: -${OPTARG}" >&2
				  echo
				  echo "[Options]"
				  echo "  Input: -l [url], -o [output], -a [y/n]"
				  echo "  Open: -u (Update), -h (Help)"
				  exit 1
				  ;;
				:)
				  echo "Option -$OPTARG requires an argument." >&2
				  exit 1
				  ;;
				h) 
					echo "[Options]"
					echo "  -l (l for link), video url."
					echo "  -o (o for output), name of the output directory."
					echo "  -a (a for audio only), [y/n] answer."
					echo "  -u (u for update), update youtube-dl with pip3"
					echo "  -h (h for help), display options."
					return 0
					;;
			esac
		done
	fi
	# if url not set take ask user input.
	if [[ -z "${url}" ]]; then
		read -p "Link: " url
	fi
	
	# if outdir not set take ask user input.
	if [[ -z "${outdir}" ]]; then
		read -p "Output directory: " outdir
	fi
	
	# if selection not set take ask user input.
	if [[ -z "${selection}" ]]; then
		read -p "Audio only? [y/n]: " selection
	fi

	if [[ ${selection} == "n" ]]; then
		dir=/home/$USER/Videos/ytv/"${outdir}"
		if [[ ! -e ${dir} ]]; then
			mkdir ${dir}
		fi
		echo "Saving to ${dir}"
		youtube-dl -f worst --restrict-filenames --output "${dir}/%(uploader)s-%(title)s" "${url}"
	else
		dir=~/Music/ytm/"${outdir}"
		if [[ ! -e ${dir} ]]; then
			mkdir ${dir}
		fi
		echo "Saving to ${dir}"
		youtube-dl --restrict-filenames --extract-audio --audio-format 'mp3' --output "${dir}/%(title)s.%(ext)s" "${url}"
	fi
	OPTIND=1 # reset flags
}

## tcol
### Display all terminal color combination.
tcol(){
	reset='\e[0m'
	echo "Reset: ${reset}"
	echo
	for x in {0..0}; do 
		for i in {30..37}; do 
		    for a in {40..47}; do 
		        echo -ne "\e[$x;$i;$a""m\\\e[$x;$i;$a""m\e[0;37;40m "
		    done
		    echo
		done
	done
}

## hex
### Preview hex color in the terminal
hex() {
    perl -e 'foreach $a(@ARGV){print "\e[48:2::".join(":",unpack("C*",pack("H*",$a)))."m \e[49m "};print "\n"' "$@"
}

## mic
### Record microphone only
mic() {
	dir=~/Music/mic
	
	# Take a file format as the second argument
	if [ -z "$2" ]; then
		format=".wav"
	else
		format="$2"
	fi
	
	if [[ ! -e $dir ]]; then
		mkdir $dir
	else
		rec ~/Music/mic/$1$format
	fi
}

## ext
### Default to apropriete file extraction programme.
ext() {
   if [ -f $1 ] ; then
       case $1 in
           *.tar.bz2)   tar xvjf $1    ;;
           *.tar.gz)    tar xvzf $1    ;;
           *.bz2)       bunzip2 $1     ;;
           *.rar)       unrar x $1       ;;
           *.gz)        gunzip $1      ;;
           *.tar)       tar xvf $1     ;;
           *.tbz2)      tar xvjf $1    ;;
           *.tgz)       tar xvzf $1    ;;
           *.zip)       unzip $1       ;;
           *.Z)         uncompress $1  ;;
           *.7z)        7z x $1        ;;
           *)           echo "don't know how to extract '$1'..." ;;
       esac
   else
       echo "'$1' is not a valid file!"
   fi
}

## wifo
### List wifi info
wifo(){
  # nmcli d wifi list
	local myssid="$(iwgetid -r)"
	echo
	echo -ne "  Wifi name:  [\e[0;34m${myssid}\e[0m]\n"
	if ! [[ "$myssid" == "" ]]; then
		echo -ne "  Status:     [\e[0;32mONLINE\e[0m]\n"
	else 
		echo -ne "  Status:     [\e[0;31mOFFLINE\e[0m]\n"
	fi
	echo
}

## memory
### Display main disk free space
memory(){

	echo
	df -BG | sed -n '4p'| awk '{print "  Disk name:  [\033[34m" $1 "\033[0m]\n" "  Free space: [" "\033[1;32m" $4 "\033[0m" "/" "\033[34m" $2 "\033[0m" "] \n  Used space:" " [\033[1;31m" $3 "\033[0m]" "(\033[34m" $5 "\033[0m)"  }'
	echo
	
}

## bat
### List batterie info
bat(){
	local low=50
	local pour_cent=$(acpi | awk '{print $4}')
	
	echo
	
	# pourcent
	pour_cent=$(echo ${pour_cent} | sed 's/,//')
	if [ ${pour_cent//[!0-9]/} -gt $low ]; then
		echo -ne "  Battery:    [\e[0;32m${pour_cent}\e[0m]\n"
	else
		echo -ne "  Battery:    [\e[0;31m${pour_cent}\e[0m]\n"
	fi
	
	pour_cent=${pour_cent//[!0-9]/}
	local pour_ten=$((pour_cent / 10))
	
	local line=""
	for dot in {1..10} 
	do
		if [[ $dot -le $pour_ten ]]; then
			line="$line\e[0;32m▮\e[0m"
		else
			line="$line\e[0;31m▯\e[0m"
		fi
	done
	echo -ne "  Power:      [$line]\n"
	
	# status
	status=$(acpi | awk '{print $3}')
	status=${status::-1}
	echo -ne "  Status:     [\e[0;34m${status}\e[0m]\n"
	
	# remaining
	remaining=$(acpi | awk '{print $5}')
	if ! [[ "${remaining}" == "" ]]; then
		echo -ne "  Remaining:  [\e[0;34m${remaining}\e[0m]\n"
	fi
	
	echo
}

## fdrop
### remove duplicate frame from video
fdrop(){
	FRAME_RATE=30
	# This will generate a console readout showing which frames the filter thinks are duplicates.
	ffmpeg -i input.mp4 -vf mpdecimate -loglevel debug -f null -
	# To generate a video with the duplicates removed
	ffmpeg -i input.mp4 -vf mpdecimate,setpts=N/${FRAME_RATE}/TB out.mp4
}
