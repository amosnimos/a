# By amosnimos 2021
# https://gitlab.com/amosnimos

#### [PRIVATE]

## li
### last installed package
li(){
	awk '$3~/^install$/ {print $4;}' /var/log/dpkg.log
}

## backup
### backup directory/file to destination
backup(){
	drive_path=/dev/sdb1
	mount_path=~/.backup
	origin_path="$(pwd)"
	if [[ $# -gt 0 ]]; then
		origin_path="$1"
	elif [[ $# -gt 1 ]]; then
		echo "Too many arguments given, expected one or less."
		return 0
	fi
	if [ "${origin_path}" = "/home/$USER" ]; then
		echo -ne "\e[0;30;41mError: This is the User directory.\e[0m\n"
		return 0
	elif [ "${origin_path}" = "/" ]; then
		echo - ne "\e[0;30;41mError: This is the Root directory.\e[0m\n"
		return 0
	fi
		
	if ! [ "$(ls -A ${mount_path})" ]; then
		echo "Mounting: [${drive_path}] to [${mount_path}]." 
		sudo mount -t auto "${drive_path}" "${mount_path}"
	fi
	if [ "$(ls -A ${mount_path})" ]; then
		echo "Backup: [${origin_path}] to [${mount_path}/backup]."
		read -p "Are you sure, you whant to proceed? [y/N]: " response
		case "$response" in
    	[yY][eE][sS]|[yY]) 
				sudo cp -r "${origin_path}" "${mount_path}/backup" && echo "${origin_path} backup completed."
				;;
			*) 
				echo "\e[0;30;43mBackup of [${origin_path}] to [${mount_path}/backup] aborted.\e[0m\n"
				;;
		esac
		
		echo "Unmounting: [$drive_path]."
		read -p "Are you sure, you whant to proceed? [y/N]: " response
		case "$response" in
	  	[yY][eE][sS]|[yY]) 
				sudo umount "$drive_path"
				;;
			*) 
				echo "\e[0;30;43mUnmounting: [$drive_path] aborted.\e[0m\n"
				;;
		esac
	fi
}

