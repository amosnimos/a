#!/bin/bash
# Display date and time
function clock(){
	yellow=$'\e[0;33m'
	green=$'\e[1;32m'
	red=$'\e[0;91m'
	reset=$'\e[0m'
	blue=$'\e[0;94m'
	counter=0
	old=""
	if ! [ $1 == "-m" ]; then
		clear
		echo
		echo "  [$blue$(date +%A" "%d" "%B" "%G)$reset]"
		while true; do
			if [ "$old" != "$(date +%H:%M)" ];then
				echo "  [$red$(date +%r)$reset][$green$counter$reset]" #[$green$(date +%D)$reset]
				old="$(date +%H:%M)"
				counter=$((counter+1))
			fi
			#sleep 30
			
		# In the following line -t for timeout, -N for just 1 character
			read -t 0.25 -N 1 input
			if [[ $input = "q" ]] || [[ $input = "Q" ]]; then
		# Quit the programme.
				  clear
				  break 
			fi
		done
	else 
		while true; do
			if [ "$old" != "$(date +%H:%M)" ];then
				clear
				echo
				echo "  [$blue$(date +%m-%d-%y)$reset]"
				echo "  [$red$(date +%H:%M)$reset]" #[$green$(date +%D)$reset]
				old="$(date +%H:%M)"
			fi
		# In the following line -t for timeout, -N for just 1 character
			read -t 0.25 -N 1 input
			if [[ $input = "q" ]] || [[ $input = "Q" ]]; then
		# Quit the programme.
				  clear
				  break 
			fi
		done
	fi
}
