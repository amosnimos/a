# By amosnimos 2021
# https://gitlab.com/amosnimos

#### [SOURCES]

# [ALIAS]
source  ~/.shell/alias.sh

# [Function]
source ~/.shell/func.sh

# [SCRIPT]:
source ~/.shell/ytst.sh
source ~/.shell/clock.sh

# [PRIVATE]
## NOTE: For my personal usage, would be bloat in the main function file, it contain path specific to my setup.
source ~/.shell/pfunc.sh
