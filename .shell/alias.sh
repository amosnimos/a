# By amosnimos 2021
# https://gitlab.com/amosnimos/a

# Default bashrc access path:
#  Shell file should be in ~/.shell 
#  Python file should be in ~/.python


# Folder path variables
pico8_path="~/Documents/pico8/pico-8/pico8"
cube2_path="cd ~/Documents/manual_install/sauerbraten/ && ./sauerbraten_unix"
dcdp_path="py ~/Documents/python/dcdp.py"
ani_path="~/Documents/github/ani-cli/ani-cli"
tpb_path="~/Documents/programme/tpb-dl-main/tpb_dl.py"
novel_path="~/.python/manga2txt.py"

# Export default file programme.
export EDITOR='nano'
export VISUAL_EDITOR='gedit'
#export IMAGE_VIWER='feh'

# Programme name
alias py="python3"
alias pip="pip3"
alias www="bollux"
alias blueman="blueman-manager"
alias moc="mocp"
alias wifi="nmtui"
alias plan="calcurse"

# Game name
alias pico8=$pico8_path
alias cube2=$cube2_path
alias supertux="supertux2"
alias editcube="sauerbraten"
alias tetris="bastet"

# Command
alias syfo="echo 'System info:' ; wifo ; bat; memory"
alias dcdp=$dcdp_path
alias ani=$ani_path
alias la='ls -A'
alias q="exit"
alias cd..="cd .."
alias cls="clear"
alias tab="gnome-terminal --tab"
alias empty-trash="rm -rf ~/.local/share/Trash/*"
alias bashrc="nohup $VISUAL_EDITOR ~/.bashrc &> /tmp/nohup.out & disown && clear"
alias awerc="nohup $VISUAL_EDITOR ~/.config/awesome/rc.lua &> /tmp/nohup.out & disown ; clear"
alias cubelan="sauerbraten-server -nLOCAL_LAN_SERVER -c5 -mmasterserver"
alias myip="hostname -I | awk '{print $1}'"
alias closepc="/sbin/shutdown +0"
alias p2t="pdftotext -layout"
alias exe="chmod +x"
alias vioff="set -o emacs"
alias vion="set -o vi"
alias ranger='ranger --choosedir=$HOME/.rangerdir; LASTDIR=`cat $HOME/.rangerdir`; cd "$LASTDIR"'
alias copy='xsel -ib'
alias app="sudo apt-get"
alias inst="sudo apt update && sudo apt autoremove && sudo apt upgrade && sudo apt install"
alias note="nano ~/.note.md"
alias ???="less ~/.config/pdata/.p.txt"
alias uplab="~/.shell/uplab.sh"

# Python file
alias novel="py $novel_path"
alias tpb="py $tpb_path"
