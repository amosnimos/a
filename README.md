# A

![](.data/.img/scr_01.png)

Project-__A__ is the code name of my first Gitlab project.
It is a backup for my main config and script. 


## Installation
The installer for this project is __~/.shell/x.sh__, make sure it is executable 
with:

> chmod +x ~/.shell/x.sh

## Usage
Most function are in the __~/.shell/func.sh__ file which with other function is sourced 
in the __~/.shell/src.sh__ file wich itself is sourced directly in the __~/.bashrc__
with the __~/.shell/defrc.sh__ which contain the default bashrc config.

## Support
Share this project. keep the credit and source link at the top of the file.

## Roadmap
So far no release planed, this project is still in it's early phase, it is more a personnal backup then anything else at the moment, so expect nothing.

## Contributing
I am open to contribution if they are justified and contribute to the improvement of this project.

## License
For free open source projects and personal use.

