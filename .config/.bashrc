# By amosnimos 2021
# https://gitlab.com/amosnimos/a

# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)

## [DEFAULT CONFIG] -->

# [SOURCES]:
source ~/.shell/defrc.sh
source ~/.shell/src.sh

## [CUSTUM CONFIG] -->

# [KEYS]:

# Replace right shift with tild 
	xmodmap -e 'keycode 62 = 0x007e'
# Replace right control with grave
	#xmodmap -e "keycode 105 = grave"
# Swap caps and escape
	#setxkbmap -option "caps:swapescape"

# Allow copy from gedit to permanent clipboard
# parcelite
